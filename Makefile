# SPDX-FileCopyrightText: 2022 Daniel Aleksandersen <https://www.daniel.priv.no/>
# SPDX-License-Identifier: GPL-3.0-only

PREFIX ?= /usr/local
BINDIR = $(PREFIX)/bin

CC ?= clang
CFLAGS += -Wall
LDFLAGS += -lm

default: sparseseek

sparseseek: sparseseek.c
	$(CC) $(CFLAGS) $(LDFLAGS) $< -o $@

install: sparseseek
	install -D -m 0755 $< $(BINDIR)/sparseseek

checks:
	clang-tidy sparseseek.c
	reuse lint

clean:
	-rm sparseek
