<!--
SPDX-FileCopyrightText: 2022 Daniel Aleksandersen <https://www.daniel.priv.no/>
SPDX-License-Identifier: GPL-3.0-only
-->

# sparseseek

Determine if a file is stored sparsely ([sparse files](https://www.ctrl.blog/entry/sparse-files.html)). Finds how much of a file is holes (sparse) and data. Compatible with recent versions of Linux, BSD, and Mac.
